If nothing other written all files in this repository are licensed under 
CC BY-NC-ND 3.0. The licensing is ONLY other if written in the file or subdirectory.
So far only one of the examples is licensed in a other way. 

see https://creativecommons.org/licenses/by-nc-nd/3.0/ for more information 

If you would like to use it commercial please contact me.

Copyright (c) 2015, al1
All rights reserved.

