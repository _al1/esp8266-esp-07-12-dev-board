![image](https://cdn.hackaday.io/images/2388151431788373056.JPG)

Some files for a projekct i have published on [hackaday.io](https://hackaday.io/project/5231-esp8266-esp-0712-dev-board) This is a small Development Board for Modules with the ESP8266 SoC.

###Some Specs:
* usable for diffenrent ESP Modules:
	* ESP-01
	* ESP-07
	* ESP-12
	* ESP-12E (SPI pins only supported by Rev2)
* on-board power management with optional Lipo battery circuit
* circuit to rest an to enter programm mode
* on-board USB to serial converter ic

Take a look at the [hackaday.io](https://hackaday.io/project/5231-esp8266-esp-0712-dev-board) site for further information.
