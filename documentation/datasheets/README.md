##links to data sheets on manufacturer sites:

###Power management

[TI LM1117-3.3 3.3V voltage regulator](http://www.ti.com/lit/ds/symlink/lm1117-n.pdf)

[TI LM3671MF-3.3 3.3V switching voltage regulator](http://www.ti.com/lit/ds/symlink/lm3671.pdf)

[Microchip MCP1700 LDO](http://ww1.microchip.com/downloads/en/DeviceDoc/20001826C.pdf) 

alternative: [Torrex XC6206 LDO](http://www.torex.co.jp/english/products/voltage_regulators/data/XC6206.pdf)

###Lipo management

[Microchip MCP73831 Lipo charge IC](http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf)

[Fortune DW01-P Battery Protection IC](http://www.ic-fortune.com/upload/Download/DW01-P-DS-15_EN.pdf)

### other

[ON BSS138 Power N-Channel Mosfet](http://www.onsemi.com/pub_link/Collateral/BSS138LT1-D.PDF)

[Fortune FS8205A dual N-Channel Mosfet](http://www.ic-fortune.com/upload/Download/FS8205A-DS-12_EN.pdf)