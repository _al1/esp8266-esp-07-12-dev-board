// Simple Blik example for ESP8266 Module. Very similar and based to/on normal Arduino example.

#define LED_PIN 2            // use GPIO2 to let the onboard user led blink

void setup() {
  pinMode(LED_PIN, OUTPUT);  // use LED_PIN as output      
  Serial.begin(9600);        // start serial to set the RX and TX pins to output and high (RX,TX leds are off then)
  Serial.println("Hello");   // just for testing
}

void loop() {
  digitalWrite(LED_PIN, HIGH);  
  delay(500);                // wait 
  digitalWrite(LED_PIN, LOW);   
  delay(500);                // wait 
}
