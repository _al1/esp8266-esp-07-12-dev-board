/* 
Simple IOT light example for EPS8266 and Arduino IDE. A Led connected to IO_12 can 
be controlled via website stored on the ESP8266. The ESP8266 is an access point.To 
use  this in your local wifi you have to change some lines. On the serial interface 
(115200; 8N1) you can see some debug information.

This sketch is based on the example code for the ESP8266 and the Arduino IDE! See 
also there for further information. 

check: https://hackaday.io/project/5231

*/

#include <ESP8266WiFi.h>
#include "html.h"

#define OUTPIN 2
#define AP_SSID "ESP8266_light"		// Hard coded SSID and PW
#define AP_PW "light123"

char check=0;

WiFiServer server(80);  //server at Port 80
WiFiClient client;

void apSeverSetup()
{
  WiFi.mode(WIFI_AP);
  Serial.println("");
  WiFi.softAP(AP_SSID/*, AP_PW*/);
  Serial.println("AP started");
  
  server.begin();
  Serial.println("Server started");
  Serial.print  ("SSID: ");
  Serial.println(AP_SSID);
  Serial.print  ("PW: ");
  Serial.println(AP_PW);			// not safe! delete this line later !!!!!!
  Serial.print  ("IP: ");
  Serial.println(WiFi.softAPIP());
  Serial.println("");
}

void setup() 
{
  pinMode(OUTPIN, OUTPUT);
  digitalWrite(OUTPIN, 0);
  Serial.begin(115200);
  delay(10);
  apSeverSetup();
}

void loop() 
{
  client = server.available();	// check if new client available
  if (!client) 
  {
    return;
  }
  
  Serial.print("\nNew Client. IP: ");
  Serial.println(client.remoteIP());
  while(!client.available())  	// wait for client
  {
    delay(1);
  }
 
  String req="";  // empty the string
  do 			  // read request
  {
    req = client.readStringUntil('\n');
    Serial.print(req);
  } while (req !="\r");
  
  Serial.print("Inhalt:");
  req = client.readStringUntil('\r');
  Serial.print(req);
  Serial.println("END");
    
  if (req=="led=0&led=1")
    check=1;
  else if (req="led=0")
    check=0;
    
  digitalWrite(OUTPIN, check);
  client.flush();
  htmlSend(check);
  delay(1);
  client.stop();
  Serial.println("Client disonnected");
}
